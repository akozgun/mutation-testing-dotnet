using System;
using Xunit;
using FluentAssertions;

namespace MutationTesting.CalculatorApp.Tests;

public class CalculatorTests
{
    private readonly Calculator _sut;
    
    public CalculatorTests()
    {
        _sut = new Calculator();
    }

    [Theory]
    [InlineData(3, 0, 3)]
    public void ShouldAdd(int number1, int number2, int expected)
    {
        var actual = _sut.Add(number1, number2);
        actual.Should().Be(expected);
    }

    [Theory]
    [InlineData(3, 3, 0)]
    public void ShouldSubtract(int number1, int number2, int expected)
    {
        var actual = _sut.Subtract(number1, number2);
        actual.Should().Be(expected);
    }

    [Theory]
    [InlineData(1, 1, 1)]
    public void ShouldMultiply(int number1, int number2, int expected)
    {
        var actual = _sut.Multiply(number1, number2);
        actual.Should().Be(expected);
    }

    [Theory]
    [InlineData(1, 1, 1, 0)]
    public void ShouldDivide(int number1, int number2, int expected, int remainder)
    {
        var actual = _sut.Divide(number1, number2);
        actual.Result.Should().Be(expected);
        actual.Remainder.Should().Be(remainder);
    }
    
    [Theory]
    [InlineData(1, 0)]
    public void ShouldDivideThrowForDivisionByZero(int number1, int number2)
    {
        var actual = () => _sut.Divide(number1, number2);
        actual.Should().Throw<DivideByZeroException>();
    }
}