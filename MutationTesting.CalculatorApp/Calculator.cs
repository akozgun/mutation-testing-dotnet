﻿namespace MutationTesting.CalculatorApp;

public class Calculator
{
    public int Add(int input1, int input2)
    {
        return input1 + input2;
    }

    public int Subtract(int input1, int input2)
    {
        return input1 - input2;
    }

    public int Multiply(int input1, int input2)
    {
        return input1 * input2;
    }

    public (int Result, int Remainder) Divide(int input1, int input2)
    {
        if (input2 == 0)
            throw new DivideByZeroException();

        return (input1 / input2, input1 % input2);
    }
}