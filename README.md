# Mutation Testing Dotnet
This project contains a sample calculator class library and unit test project for a brief overview of mutation testing with Stryker in a .NET application.

## Getting started
In order to run tests with code coverage report:

```
dotnet test --collect:"XPlat Code Coverage"
```

To generate code coverage report (Replace {GUID} with the actual guid from the test result's attachments):
```
reportgenerator -reports:"MutationTesting.CalculatorApp.Tests/TestResults/{GUID}/coverage.cobertura.xml" -targetdir:"coveragereport" -reporttypes:Html
```

For more information about the coverlet and tooling, please check the following guide:
[Use code coverage for unit testing](https://docs.microsoft.com/en-us/dotnet/core/testing/unit-testing-code-coverage?tabs=linux#code-coverage-tooling)

## Stryker.NET

### What is Stryker
Stryker offers mutation testing for your .NET Core and .NET Framework projects. It allows you to test your tests by temporarily inserting bugs in your source code.

You can visit the following links for more information:

* [Documentation](https://stryker-mutator.io/)
* [Github](https://github.com/stryker-mutator/stryker-net)

### Usage
Stryker.NET can be installed as a global tool or project dependency (so that it can be wired up to CI pipeline). The tooling has already been added to this project as a dependency.

In the path contains the unit test project following command will generate the report.

```
dotnet stryker
```
